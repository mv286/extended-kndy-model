close all;
clear all;

%% set KNDy parameters
params.d1 = 0.2;
params.d2 = 1;
params.d3 = 10; 
params.p1 =4;
params.p2 = 40;
params.p3 = 2e-3;
params.maxRate = 30000;
params.p3Basal =  0.02; 
params.KD = 0.3; 
params.KN = 4;
params.Kr1 = 600;
params.Kr2 = 200;

params.ee = 0;
params.p1_basal = 0;
params.p2_basal = 0;
params.KI = 2;

%% set MePD parameters

params.d_E = 1;
params.k_E = 1;
params.r_E = 1;
params.a_E = 2.;
params.theta_E = 3.52;

params.d_I = 1;
params.k_I = 1;
params.r_I = 1;
params.a_I = 2.17;
params.theta_I = 3.6;

params.c1 = .5;
params.c2 = 0;
params.c3 = 0.;
params.c4 = 0.;
params.c5 = 50;
params.c6 = 10;
params.c7 = 1.;
params.c8 = 1.;
params.kiss = 1;
kiss_p = 2;

%% simulation parameters 
params.Tmax =629;
params.Dt =0.1;
params.Tspan = 0:params.Dt:params.Tmax;
params.dim = 6;
params.opt = [];
period_val = [0, 0];

paramstmp = params;

tmax2 =90;


%% A) BASELINE
test_i = 1;

% simulate
X0 = zeros(1, params.dim); 
[T, Y] = ode23s(@(t,y) extended_model_v1(t,y,params), params.Tspan , X0, params.opt);
X0 = Y(end,:);

% plot results
figure(test_i);
tt = (params.Tmax-150);
plot(T(T>=tt) - tt , Y(T>=tt ,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')

%calculate period and duty cycle
ft = (round(size(Y, 1)/2)):size(Y, 1);
timecourse_tmp = mean(Y(ft,6),2) ;
ac = xcorr(timecourse_tmp,timecourse_tmp);  
[~,loc] = findpeaks(ac./max(ac), 'MinPeakHeight',0.1);
DT = params.Tspan(2)-params.Tspan(1);
if isempty(max(diff(loc)*DT))        
    period_val(test_i) = 0 ;  
else
    period_val(test_i) = max(diff(loc)*DT) ;    
end

tt = (params.Tmax-60);
Ybaseline = Y(T>=tt,:);
Tbaseline = T(T>=tt)-tt;


%% B)  +OPTIC STIMULATION
test_i = 2;

params = paramstmp;
params.kiss =  kiss_p;
params.Tspan = 0:params.Dt:tmax2;

% run simulation
Y0 = Ybaseline;
[T, Y] = ode23s(@(t,y) extended_model_v1(t,y,params), params.Tspan , Y0(end,:), params.opt);

T = [Tbaseline(1:end-1) ; T+Tbaseline(end)];
Y = [Ybaseline(1:end-1,:) ; Y];

% plot results
figure(test_i);
plot(T, Y(:,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')


%calculate period and duty cycle
ft = (round(size(Y, 1)/2)):size(Y, 1);
timecourse_tmp = mean(Y(ft,6),2) ;
ac = xcorr(timecourse_tmp,timecourse_tmp);  
[~,loc] = findpeaks(ac./max(ac), 'MinPeakHeight',0.1);
DT = params.Tspan(2)-params.Tspan(1);
if isempty(max(diff(loc)*DT))        
    period_val(test_i) = 0 ;  
else
    period_val(test_i) = max(diff(loc)*DT) ;    
end


%% C)  +GABA antagonist
test_i = 3;

params = paramstmp;
params.c2 = 0;
params.c4 = 0;
params.c6 = 0;
params.Tspan = 0:params.Dt:tmax2;

% simulate
Y0 = Ybaseline;
[T, Y] = ode23s(@(t,y) extended_model_v1(t,y,params), params.Tspan , Y0(end,:), params.opt);

T = [Tbaseline(1:end-1) ; T+Tbaseline(end)];
Y = [Ybaseline(1:end-1,:) ; Y];

% plot results
figure(test_i);
plot(T, Y(:,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')

%calculate period and duty cycle
ft = (round(size(Y, 1)/2)):size(Y, 1);
timecourse_tmp = mean(Y(ft,6),2) ;
ac = xcorr(timecourse_tmp,timecourse_tmp);  
[~,loc] = findpeaks(ac./max(ac), 'MinPeakHeight',0.1);
DT = params.Tspan(2)-params.Tspan(1);
if isempty(max(diff(loc)*DT))        
    period_val(test_i) = 0 ;  
else
    period_val(test_i) = max(diff(loc)*DT) ;    
end


%% D)  +Glut antagonist
test_i = 4;

params = paramstmp;
params.c1 = 0;
params.c3 = 0;
params.c5 = 0;
params.Tspan = 0:params.Dt:tmax2;

%simulate
Y0 = Ybaseline;
[T, Y] = ode23s(@(t,y) extended_model_v1(t,y,params), params.Tspan , Y0(end,:), params.opt);


T = [Tbaseline(1:end-1) ; T+Tbaseline(end)];
Y = [Ybaseline(1:end-1,:) ; Y];

%plot results
figure(test_i);
plot(T, Y(:,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')

%calculate period and duty cycle
ft = (round(size(Y, 1)/2)):size(Y, 1);
timecourse_tmp = mean(Y(ft,6),2) ;
ac = xcorr(timecourse_tmp,timecourse_tmp);  
[~,loc] = findpeaks(ac./max(ac), 'MinPeakHeight',0.1);
DT = params.Tspan(2)-params.Tspan(1);
if isempty(max(diff(loc)*DT))        
    period_val(test_i) = 0 ;  
else
    period_val(test_i) = max(diff(loc)*DT) ;    
end

%% E)  + GABA antagonist + optic stimulation
test_i = 5;

params = paramstmp;
params.Tspan = 0:params.Dt:tmax2;
params.c2 = 0;
params.c4 = 0;
params.c6 = 0;
params.kiss =  kiss_p;

%simulate
Y0 = Ybaseline;
[T, Y] = ode23s(@(t,y) extended_model_v1(t,y,params), params.Tspan , Y0(end,:), params.opt);

T = [Tbaseline(1:end-1) ; T+Tbaseline(end)];
Y = [Ybaseline(1:end-1,:) ; Y];

%run simulations
figure(test_i);
plot(T, Y(:,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
%set(gca, 'xlim', [0 params.Tspan(end)])
print(gcf, ['fig_' num2str(test_i)], '-depsc2')

%calculate period and duty cycle
ft = (round(size(Y, 1)/2)):size(Y, 1);
timecourse_tmp = mean(Y(ft,6),2) ;
ac = xcorr(timecourse_tmp,timecourse_tmp);  
[~,loc] = findpeaks(ac./max(ac), 'MinPeakHeight',0.1);
DT = params.Tspan(2)-params.Tspan(1);
if isempty(max(diff(loc)*DT))        
    period_val(test_i) = 0 ;  
else
    period_val(test_i) = max(diff(loc)*DT) ;    
end


%% F)  + Glut antagonist + optic stimulation
test_i = 6;

params = paramstmp;
params.Tspan = 0:params.Dt:tmax2;
params.c1 = 0;
params.c3 = 0;
params.c5 = 0;
params.kiss =  kiss_p;

Y0 = Ybaseline;
[T, Y] = ode23s(@(t,y) extended_model_v1(t,y,params), params.Tspan , Y0(end,:), params.opt);

T = [Tbaseline(1:end-1) ; T+Tbaseline(end)];
Y = [Ybaseline(1:end-1,:) ; Y];

figure(test_i);
plot(T, Y(:,6), 'k', 'linewidth', 1.5); 
set(gca,'fontsize',20);
set(gcf,'paperposition',[0 0 1.6 1.1]*10);
set(gca, 'ylim', [0 3000])
set(gca, 'xtick', [0:20:140]);
print(gcf, ['fig_' num2str(test_i)], '-depsc2')

%calculate period and duty cycle
ft = (round(size(Y, 1)/2)):size(Y, 1);
timecourse_tmp = mean(Y(ft,6),2) ;
ac = xcorr(timecourse_tmp,timecourse_tmp);  
[~,loc] = findpeaks(ac./max(ac), 'MinPeakHeight',0.1);
DT = params.Tspan(2)-params.Tspan(1);
if isempty(max(diff(loc)*DT))        
    period_val(test_i) = 0 ;  
else
    period_val(test_i) = max(diff(loc)*DT) ;    
end
%%

period_val